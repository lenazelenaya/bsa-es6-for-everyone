import { showWinnerModal } from "./modals/winner";

export function fight(firstFighter, secondFighter) {
  let winner;
  let firstFighterHealth = firstFighter.health;
  let secondFighterHealth = secondFighter.health;
  let damageOfFirst, damageOfSecond;

  while (firstFighterHealth > 0 && secondFighterHealth > 0){
        damageOfFirst = getDamage(firstFighter, secondFighter);
        damageOfSecond = getDamage(secondFighter, firstFighter);
        firstFighterHealth -= damageOfSecond;
        secondFighterHealth -= damageOfFirst;
  }

  return firstFighterHealth <= 0 ? secondFighter : firstFighter;
}

export function getDamage(attacker, enemy) {
  const damage = getHitPower(attacker) - getBlockPower(enemy);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  const chance = Math.random() + 1;
  return fighter.attack * chance;
}

export function getBlockPower(fighter) {
  const chance = Math.random() + 1;
  return fighter.defense * chance;
}

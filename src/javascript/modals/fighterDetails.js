import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter) {
  const title = createElement({ tagName: 'span', className: 'modal-header'});
    title.innerText ='Fighter info';
  const bodyElement = createElement({ tagName: 'span', className: 'modal-body'});
    bodyElement.append(createFighterDetails(fighter));

  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  
  const { name, health, attack, defense, source } = fighter;
  const fighterDetails = createElement({ tagName: 'div', className: 'fighter-info' });

  const nameElement = createElement({ tagName: 'span', className: 'fighter-characteristic' });
  const healthElement = createElement({ tagName: 'span', className: 'fighter-characteristic' });
  const attackElement = createElement({ tagName: 'span', className: 'fighter-characteristic' });
  const defenseElement = createElement({ tagName: 'span', className: 'fighter-characteristic' });
  
  const attributes = { src: source, title: name, alt: name };
  const imgElement = createElement({ tagName: 'img', className: 'fighter-img', attributes });

  nameElement.innerHTML = `<strong>Name</strong>: ${name} `;
  healthElement.innerHTML = `<strong>Health</strong>: ${health} `;
  attackElement.innerHTML = `<strong>Attack</strong>: ${attack} `;
  defenseElement.innerHTML = `<strong>Defense</strong>: ${defense}`;
  
  fighterDetails.append(imgElement, nameElement, healthElement, attackElement, defenseElement);

  return fighterDetails;
}
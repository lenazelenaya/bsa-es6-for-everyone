import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showAlert() {
    const title = createElement({ tagName: 'span', className: 'modal-header'});
    title.innerText ='Amount of fighters is too big';

    const text = createElement({ tagName: 'span', className: 'fighter-info'});
    text.innerText = 'Please, check two fighters.';

    const bodyElement = createElement({ tagName: 'div', className: 'modal-body'});
    bodyElement.append(text);

  showModal({ title, bodyElement });
}
import { showModal } from './modal';
import { createElement } from '../helpers/domHelper';

export function showWinnerModal(fighter) {
  const title = createElement({ tagName: 'span', className: 'modal-header'});
    title.innerText = `GAME OVER`;

  const bodyElement = createElement({ tagName: 'div', class: 'modal-body' });
    bodyElement.append(createBodyElement(fighter));

  showModal({ title, bodyElement });
}

function createBodyElement(fighter){
  const { name, source } = fighter;
  const winnerDetails = createElement({ tagName: 'div', className: 'fighter-info' });

  const element = createElement({ tagName: 'span', className: 'winner' });
    element.innerHTML= `<strong>Winner</strong>: ${name}!!!`;

  const attributes = { src: source, title: name, alt: name };
  const imgElement = createElement({ tagName: 'img', className: 'fighter-img', attributes });

  winnerDetails.append(element, imgElement);

  return winnerDetails;
}
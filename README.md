# Third homework for BSA

## This homework includes these tasks:

* В файле ```fighterService``` создать функцию для получения детальной информации для бойца. Добавить возможность ее просмотра. Для этого реализовать функцию ```getFighterInfo```, которая бы обработала клик по бойцу. Данные отобразить с помощью функции ```showFighterDetailsModal```.
* Создать функцию ```fight```, которая принимает как параметры так и зупускает процесс игры. Имя победителя выводится на экран с помощью функции ```showWinnerModal```.
* Реализовать функции:
	* ```getDamage```, расчет урона здоровья соперника
	* ```getHitPower```, расчет силы удара 
	* ```getBlockPower```, расчет силы блока (амортизация удара соперника) 
	

## Links
* [Street-fighters](http://sf-zelly.com.s3-website-us-east-1.amazonaws.com)
